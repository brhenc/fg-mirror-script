The Gentoo distfiles mirrors are superb, but they drop packages a lot.
For Funtoo this is bad since we need all the source tarballs to be reachable, 
and linkrot kills a lot of ebuilds. 

The Goal of this script is to create a diff of a reference Gentoo distfile 
mirror and the master Funtoo mirror that contains all the distfiles we 
have/had in our Funtoo tree.

The diffed files are then uploaded to the rest of the Funtoo mirrors.

A separate script should handle mirroring files that are available in
the Gentoo mirrors but not in the Funtoo tree.