
.PHONY: clean mirror

fetch: ceresia_distfiles gentoo_distfiles local_distfiles

ceresia_distfiles:
	wget https://distmirror.ceresia.ch:81/distfiles -O ceresia_distfiles

gentoo_distfiles:
	wget http://gentoo.mirrors.tera-byte.com/distfiles -O gentoo_distfiles

local_distfiles:
	wget http://127.0.0.1/distfiles -O local_distfiles

compare: fetch 
	diff -u gentoo_distfiles ceresia_distfiles > cgdiff; [ $$? -eq 1 ]
	cat cgdiff | grep "^+" | sed s'/>/\n/' | grep "href=" | sed s'/<a href="//' \
	| sed s'/"//' | sed s'/+//' > cgdiff_clean
	cat local_distfiles | grep "^<a" | sed s'/>/\n/' | grep "href=" | sed s'/<a href="//' \
	| sed s'/"//' > ldiff
	diff -u ldiff cgdiff_clean > fetchlist; [ $$? -eq 1 ] 
	cat fetchlist | grep "^+" | sed s'/+/wget -r http:\/\/distmirror.ceresia.ch\/distfiles\//' \
	|  sed '/index\.html/d' | sed '/cgdiff_clean/d' > fetchlist_clean

download: compare
	cat fetchlist_clean | parallel -P5

mirror: download
	mv distmirror.ceresia.ch/distfiles/* /var/www/ceresia.ch/distfiles/distfiles/

clean:
	rm -rf ceresia_distfiles
	rm -rf gentoo_distfiles
	rm -rf local_distfiles
	rm -rf cgdiff
	rm -rf cgdiff_clean
	rm -rf ldiff
	rm -rf fetchlist
	rm -rf fetchlist_clean
	rm -rf distmirror.ceresia.ch
