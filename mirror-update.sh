#!/bin/bash
cd /var/www/ceresia.ch/distfiles-scripts/fg-mirror-script
make mirror &>> /var/log/distfiles_mirror_update.log
make clean &>> /var/log/distfiles_mirror_update.log
